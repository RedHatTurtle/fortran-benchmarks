program dgemm_bench


    ! This simple program aims to compute the matrix multiplication operation
    ! using lapack's dgemm in a big square matrix and in a series of small sq
    ! uare matrices.
    ! Test with: -mkl and -llapack -lblas implementations.
    !
    ! Author: Leonardo Motta 20/09/2016.

    implicit none


    ! General benchmark variables.

    real(kind=8) :: t1, t2, tf, wt
    integer(kind=4) :: cr, c1, c2

!    integer(kind=4) :: large_size = 5000!Set stack to unlimited size with 'ulimit -s unlimited' or allocate arrays on heap with '-heap-arrays'
    integer(kind=4) :: large_size = 1022 !Maximum size accepted by defaul on matmul
!    integer(kind=4) :: small_size = 6 !Tipping point when dgemm gets faster with arrays on stack and O3 optimizations
!    integer(kind=4) :: small_size = 5 !Tipping point when dgemm gets faster with arrays on heap  and O3 optimizations
!    integer(kind=4) :: small_size = 5 !Tipping point when dgemm gets faster with arrays on stack and O2 optimizations
!    integer(kind=4) :: small_size = 4 !Tipping point when dgemm gets faster with arrays on heap  and O2 optimizations
    integer(kind=4) :: small_size = 5
    integer(kind=4) :: number_mult = 1e7
    integer(kind=4) :: i


    ! Variables for the big matrix multiplication.

    real(kind=8), allocatable, dimension(:,:) :: matrix_A
    real(kind=8), allocatable, dimension(:,:) :: matrix_B
    real(kind=8), allocatable, dimension(:,:) :: matrix_R


    ! Output performance file.

    open(1,file="perf_dgemm.dat")

    !--------------------------------------------------------------------------!
    !          TESTING OUT THE MULTIPLICATION OF A SINGLE BIG MATRIX           !
    !--------------------------------------------------------------------------!

    write(1,*) "Testing multiplication of large matrices"

    ! Initilizing time measure veriables

    t1 = 0.0d0
    t2 = 0.0d0
    tf = 0.0d0
    call system_clock(count_rate=cr)
    c1 = 0
    c2 = 0

    ! Allocating matrices.

    allocate(matrix_A(large_size,large_size))
    allocate(matrix_B(large_size,large_size))
    allocate(matrix_R(large_size,large_size))


    ! Putting randon numbers in the matrices.

    call RANDOM_NUMBER(matrix_A)
    call RANDOM_NUMBER(matrix_B)


    ! Zero out answer.

    matrix_R = 0.0d0


    ! Calling clock timer.

    call cpu_time(t1)
    call system_clock(c1)

    ! Calling Lapack's DGEMM subroutine.

    call dgemm('N','N',large_size,large_size,large_size,1.0d0, &
        matrix_A,large_size,matrix_B,large_size,1.0d0,matrix_R,large_size)


    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "   CPU  time running Lapack's dgemm on ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "   Wall time running Lapack's dgemm on ",large_size," x ",large_size," matrix = ", wt


    ! Testing matmul.

    t1 = 0.0d0
    t2 = 0.0d0
    tf = 0.0d0

    matrix_R = 0.0d0

    call cpu_time(t1)
    call system_clock(c1)


    ! Calling matmul internal routine.

    matrix_R = matmul(matrix_A, matrix_B)

    call cpu_time(t2)
    call system_clock(c2)

    tf = t2 - t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "   CPU  time running Matmul on a ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "   Wall time running Matmul on a ",large_size," x ",large_size," matrix = ", wt


    ! Finish with the single big matrix multiplication.

    deallocate(matrix_A)
    deallocate(matrix_B)
    deallocate(matrix_R)


    !--------------------------------------------------------------------------!
    !          TESTING OUT THE MULTIPLICATION MANY SMALL MATRICES              !
    !--------------------------------------------------------------------------!

    write(1,*) "Testing several multiplications of small matrices"

    ! Allocating matrices.

    allocate(matrix_A(small_size,small_size))
    allocate(matrix_B(small_size,small_size))
    allocate(matrix_R(small_size,small_size))


    ! Putting random numbers in the matrice.

    call RANDOM_NUMBER(matrix_A)
    call RANDOM_NUMBER(matrix_B)

    matrix_R = 0.0d0


    t1 = 0.0d0
    t2 = 0.0d0
    tf = 0.0d0


    call cpu_time(t1)
    call system_clock(c1)


    do i = 1, number_mult

        call dgemm('N','N',small_size,small_size,small_size,1.0d0, &
            matrix_A,small_size,matrix_B,small_size,1.0d0,matrix_R,small_size)

    end do


    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "   CPU  time running Lapack's dgemm ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "   Wall time running Lapack's dgemm ",number_mult," times on ",small_size," x ",small_size," matrices = ", wt


    ! Do the same with matmul.

    ! Putting random numbers in the matrice.

    call RANDOM_NUMBER(matrix_A)
    call RANDOM_NUMBER(matrix_B)

    matrix_R = 0.0d0


    t1 = 0.0d0
    t2 = 0.0d0
    tf = 0.0d0


    call cpu_time(t1)
    call system_clock(c1)


    do i = 1, number_mult

        matrix_R = matmul(matrix_A, matrix_B)

    end do


    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "   CPU  time running Matmul ",number_mult," times on ",small_size,"x",small_size," matrices = ", tf
    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "   Wall time running Matmul ",number_mult," times on ",small_size,"x",small_size," matrices = ", wt


    deallocate(matrix_A)
    deallocate(matrix_B)
    deallocate(matrix_R)


    close(1)

end program dgemm_bench
