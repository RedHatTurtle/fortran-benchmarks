# Benchmarks of numerical libraries #

## What's this repo ? ##
A usefull and organized library of mathematical subroutines to help you do your job more efficiently. Take a look and enjoy !

## Motivation ##

This repository main goal is to collect library and subroutines benchmarks for
lab's internal usage. Any usual operation, like tridiagonal system solver, matrix
invertion, vector copy, etc. Is welcome. The result of this organization effort
will be better comprehension of the most efficient ways of doing things in our 
typical applications.

## Rules and Good Practices ##

Since we are not dealing with high-school programming, serious programming languages
like Fortran and C are better suited for our needs. Python and MATLAB are welcome when
testing exotic algorithns but a compiled version of the code should be present.

The code should also be self-contained in one file, this way the user will have a 
very good understanding of how the subroutines calls work in full program context.
No isolated, non-commented stuff here ! Remeber that, self-contained in usage of a 
library so, you don't need to include the library files here since testing different
types of compilation of the same library would be one of the goals here.

When dealing with third marty libraries such as LAPACK or PETsc, be gentle and write
a small compilation tutorial on how to compile the program in the header of the file.

To avoid terminal mess, make your subroutines (or functions) output a datafile with 
explicit information on what is happening there !

## Would be interesting to test ##
* Different BLAS compilations when dealing with LAPACK subroutines.
* CUDA optimized libraries (include also the cost of communication).
* Our own (Lab) subroutines.

## Intersting subroutines to be tested ##
* Block-Tridiagonal solver. Compare Intel cookbook approach with our own implementations.
* Full sparse system solving.
* Scalar tridiagonal solver (LAPACK vs our own).
* Scalar tridiagonal-periodic solver (compare this one in precision when not solving the full periodic matrix.
* In operations that involve loop nesting, test the order of the loops for memory access testing.
* Test C language interfaces with LAPACK library.
* Benchmark Jack's Libraries such as PLASMA/ATLAS/LAPACK for our goals here.


