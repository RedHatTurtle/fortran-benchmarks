program QR


    ! This simple program aims to compute the matrix multiplication operation
    ! using lapack's dgemm in a big square matrix and in a series of small sq
    ! uare matrices.
    ! Test with: -mkl and -llapack -lblas implementations.
    !
    ! Author: Leonardo Motta 20/09/2016.

    use f95_precision
    use blas95
    use lapack95
    implicit none

    ! General benchmark variables

    real(kind=8) :: t1, t2, tf, wt
    integer(kind=4) :: cr, c1, c2

!    integer(kind=4) :: large_size = 4096!Set stack to unlimited size with 'ulimit -s unlimited' or allocate arrays on heap with '-heap-arrays'
    integer(kind=4) :: large_size = 102 !Maximum size accepted by defaul on matmul
    integer(kind=4) :: small_size = 3
    integer(kind=4) :: number_mult = 1e5
    integer(kind=4) :: i


    ! Variables for the big matrix multiplication

    real(kind=8), allocatable, dimension(:,:) :: matA
    real(kind=8), allocatable, dimension(:,:) :: matQR
    real(kind=8), allocatable, dimension(:,:) :: matQ
    real(kind=8), allocatable, dimension(:,:) :: vecB
    real(kind=8), allocatable, dimension(:,:) :: vec1
    real(kind=8), allocatable, dimension(:,:) :: vec2
    real(kind=8), allocatable, dimension(:,:) :: x
    real(kind=8), allocatable, dimension(:) :: tau
    integer(kind=4), allocatable, dimension(:) :: jpvt


    ! Output performance file

    open(1,file="perf_qr.dat")

    !--------------------------------------------------------------------------!
    !                  TESTING OUT WITH A SINGLE BIG MATRIX                    !
    !--------------------------------------------------------------------------!

    write(1,*) "*************************************"
    write(1,*) "Testing solving a single large matrix"
    write(1,*) "*************************************"
    write(1,*)
    write(*,*) "*************************************"
    write(*,*) "Testing solving a single large matrix"
    write(*,*) "*************************************"
    write(*,*)

    ! Initilizing time measure veriables

    t1 = 0.0d0
    t2 = 0.0d0
    tf = 0.0d0
    call system_clock(count_rate=cr)
    c1 = 0
    c2 = 0

    ! Allocating matrices

    allocate(matA(large_size,large_size))
    allocate(matQR(large_size,large_size))
    allocate(matQ(large_size,large_size))
    allocate(vecB(large_size,1))
    allocate(vec1(large_size,1))
    allocate(vec2(large_size,1))
    allocate(x(small_size,1))

    allocate(tau(large_size))
    allocate(jpvt(large_size))

    ! Putting random numbers in the matrices
    call RANDOM_SEED
    call RANDOM_NUMBER(matA)
    call RANDOM_NUMBER(vecB)

    ! Initializing auxiliary matrices
    matQR = matA
    vec1 = vecB
    vec2 = vecB

    ! Calculating the QR factorization
    call cpu_time(t1)
    call system_clock(c1)
        call geqp3(matQR, jpvt, tau)
    call cpu_time(t2)
    call system_clock(c2)

    ! Apply pivoting to the RHS
    do i=1, large_size
        vec1(i,1) = vecB(jpvt(i),1)
    enddo
    vecB = vec1

    ! Apply pivoting to the original matrix
    do i=1, large_size
        matQ(:,i) = matA(:,jpvt(i))
    enddo
    matA = matQ

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the QR factorization with LAPACK's GEQP3 on ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the QR factorization with LAPACK's GEQP3 on ",large_size," x ",large_size," matrix = ", wt
    write(1,*)

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the QR factorization with LAPACK's GEQP3 on ",large_size," x ",large_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the QR factorization with LAPACK's GEQP3 on ",large_size," x ",large_size," matrix = ", wt
    write(*,*)

    ! Calculate matrix Q explicitly
    matQ = matQR
    call cpu_time(t1)
    call system_clock(c1)
        call orgqr(matQ, tau)
    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the Q matrix with LAPACK's ORGQR on ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the Q matrix with LAPACK's ORGQR on ",large_size," x ",large_size," matrix = ", wt
    write(1,*)

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the Q matrix with LAPACK's ORGQR on ",large_size," x ",large_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the Q matrix with LAPACK's ORGQR on ",large_size," x ",large_size," matrix = ", wt
    write(*,*)

    ! Solving the system with Lapack's ORMQR subroutine
    call cpu_time(t1)
    call system_clock(c1)
        call ormqr(matQR, tau, vec1, trans='T')
        call trsm(matQR, vec1)
    call cpu_time(t2)
    call system_clock(c2)

    do i=1, large_size
        x(jpvt(i),1) = vec1(i,1)
    enddo

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time running Lapack's ORMQR on ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time running Lapack's ORMQR on ",large_size," x ",large_size," matrix = ", wt
    write(1,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time running Lapack's ORMQR on ",large_size," x ",large_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time running Lapack's ORMQR on ",large_size," x ",large_size," matrix = ", wt
    write(*,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))

    ! Solving the system by explicitly multiplying b*Q with matmul
    call cpu_time(t1)
    call system_clock(c1)
        vec2(:,1) = matmul(vecB(:,1),matQ)
        call trsm(matQR, vec2)
    call cpu_time(t2)
    call system_clock(c2)

    do i=1, large_size
        x(jpvt(i),1) = vec1(i,1)
    enddo

    tf = t2 - t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time storing the Q matrix and running Matmul on a ",large_size," x ",large_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time storing the Q matrix and running Matmul on a ",large_size," x ",large_size," matrix = ", wt
    write(1,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(1,*)

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time storing the Q matrix and running Matmul on a ",large_size," x ",large_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time storing the Q matrix and running Matmul on a ",large_size," x ",large_size," matrix = ", wt
    write(*,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(*,*)

    ! Finish with the single big matrix multiplication

    deallocate(matA)
    deallocate(matQR)
    deallocate(matQ)
    deallocate(vecB)
    deallocate(vec1)
    deallocate(vec2)
    deallocate(x)
    deallocate(tau)
    deallocate(jpvt)


    !--------------------------------------------------------------------------!
    !                 TESTING OUT WITH MANY SMALL MATRICES                     !
    !--------------------------------------------------------------------------!

    write(1,*) "**************************************"
    write(1,*) "Testing solving several small matrices"
    write(1,*) "**************************************"
    write(1,*)
    write(*,*) "**************************************"
    write(*,*) "Testing solving several small matrices"
    write(*,*) "**************************************"
    write(*,*)

    ! Allocating matrices

    allocate(matA(small_size,small_size))
    allocate(matQR(small_size,small_size))
    allocate(matQ(small_size,small_size))
    allocate(vecB(small_size,1))
    allocate(vec1(small_size,1))
    allocate(vec2(small_size,1))
    allocate(x(small_size,1))

    allocate(tau(small_size))
    allocate(jpvt(small_size))

    ! Putting random numbers in the matrices
    call RANDOM_SEED
    call RANDOM_NUMBER(matA)
    call RANDOM_NUMBER(vecB)

    matA(1,1) = 1
    matA(1,2) = 1
    matA(1,3) = 1
    matA(2,1) = 1
    matA(2,2) = 2
    matA(2,3) = 1
    matA(3,1) = 1
    matA(3,2) = 1
    matA(3,3) = 2
    vecB(1,1) = 6
    vecB(2,1) = 8
    vecB(3,1) = 9

    ! Initializing auxiliary matrices
    matQR = matA

    ! Calculating the QR factorization
    call cpu_time(t1)
    call system_clock(c1)
        call geqp3(matQR, jpvt, tau)
    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the QR factorization with LAPACK's GEQP3 on ",small_size," x ",small_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the QR factorization with LAPACK's GEQP3 on ",small_size," x ",small_size," matrix = ", wt
    write(1,*)

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the QR factorization with LAPACK's GEQP3 on ",small_size," x ",small_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the QR factorization with LAPACK's GEQP3 on ",small_size," x ",small_size," matrix = ", wt
    write(*,*)

    ! Calculate matrix Q explicitly
    matQ = matQR
    call cpu_time(t1)
    call system_clock(c1)
        call orgqr(matQ, tau)
    call cpu_time(t2)
    call system_clock(c2)

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the Q matrix with LAPACK's ORGQR on ",small_size," x ",small_size," matrix = ", tf
    write(1,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the Q matrix with LAPACK's ORGQR on ",small_size," x ",small_size," matrix = ", wt
    write(1,*)

    write(*,'(A,I5,A,I5,A,F11.7)') "CPU  time calculating the Q matrix with LAPACK's ORGQR on ",small_size," x ",small_size," matrix = ", tf
    write(*,'(A,I5,A,I5,A,F11.7)') "Wall time calculating the Q matrix with LAPACK's ORGQR on ",small_size," x ",small_size," matrix = ", wt
    write(*,*)

    ! Solving the system with Lapack's ORMQR subroutine
    call cpu_time(t1)
    call system_clock(c1)
    do i=1, number_mult
        vec1 = vecB
        call ormqr(matQR, tau, vec1, trans='T')
        call trsm(matQR, vec1)
    enddo
    call cpu_time(t2)
    call system_clock(c2)

    do i=1, small_size
        x(jpvt(i),1) = vec1(i,1)
    enddo

    tf = t2-t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "CPU  time running Lapack's ORMQR ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "Wall time running Lapack's ORMQR ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(1,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(1,*)

    write(*,'(A,I9,A,I2,A,I2,A,F11.7)') "CPU  time running Lapack's ORMQR ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(*,'(A,I9,A,I2,A,I2,A,F11.7)') "Wall time running Lapack's ORMQR ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(*,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(*,*)

    ! Solving the system by explicitly multiplying b*Q with matmul
    call cpu_time(t1)
    call system_clock(c1)
    do i=1, number_mult
        vec2(:,1) = matmul(vecB(:,1),matQ)
        call trsm(matQR, vec2)
    enddo
    call cpu_time(t2)
    call system_clock(c2)

    do i=1, small_size
        x(jpvt(i),1) = vec1(i,1)
    enddo

    tf = t2 - t1
    wt = real(c2-c1,8)/real(cr,8)

    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "CPU  time storing the Q matrix and running Matmul ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(1,'(A,I9,A,I2,A,I2,A,F11.7)') "Wall time storing the Q matrix and running Matmul ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(1,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(1,*)

    write(*,'(A,I9,A,I2,A,I2,A,F11.7)') "CPU  time storing the Q matrix and running Matmul ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(*,'(A,I9,A,I2,A,I2,A,F11.7)') "Wall time storing the Q matrix and running Matmul ",number_mult," times on ",small_size," x ",small_size," matrices = ", tf
    write(*,'(A,ES13.5)') "      L2-Norm of the residual", norm2(vecB-matmul(matA,x))
    write(*,*)

    print *, vec1
    print *, vec2
    print *, jpvt

    deallocate(matA)
    deallocate(matQR)
    deallocate(matQ)
    deallocate(vecB)
    deallocate(vec1)
    deallocate(vec2)
    deallocate(x)
    deallocate(tau)
    deallocate(jpvt)

    close(1)

end program QR
